Sprocky
=======
I'm shocked over the fact that there is not a usable and feature complete sproc wrapper for Java. So
BEHOLD SPROCKY

Sprocky is a sproc wrapper for JDBC which works for Java 6+. It intends to be easy-to-use (the major goal)
by using Convention Over Configuration extensively, feature complete with support for named parameters,
output parameters, retrieving result set returned by sproc, multiple result set support, scalar value
support, etc. Did I mention that it is supposed to be rocky solid? And it allows and encourages reusing
existing POJO classes through using IgnoreInheritedFields/SprocParameterIgnore/ResultSetColumnIgnore
annotations.

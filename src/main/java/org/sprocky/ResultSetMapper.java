package org.sprocky;

import java.sql.ResultSet;
import java.util.List;

/**
 * Author: Imgen
 */
public interface ResultSetMapper {
    <T> List<T> map(Class<T> type,
                    ResultSet resultSet,
                    NameMappingConvention nameMappingConvention,
                    TypeMapper typeMapper)
            throws Throwable;
}

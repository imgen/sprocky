package org.sprocky;

import java.util.List;

/**
 * Author: Imgen
 */
public class QuerySprocResult<T> {
    public int returnValue;
    public List<T> queryResult;
    public List<List<?>> queryResults;

    public QuerySprocResult(int returnValue, List<T> queryResult, List<List<?>> queryResults) {
        this.returnValue = returnValue;
        this.queryResult = queryResult;
        this.queryResults = queryResults;
    }
}

package org.sprocky;

/**
 * Author: Imgen
 */
public class ScalarSprocResult {
    public int returnValue;
    public Object scalarValue;

    public ScalarSprocResult(int returnValue, Object scalarValue) {
        this.returnValue = returnValue;
        this.scalarValue = scalarValue;
    }
}

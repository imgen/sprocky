package org.sprocky.util;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.sprocky.IgnoreInheritedFields;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.sprocky.util.Constants.EMPTY_STRING;

/**
 * Author: Imgen
 */
public class MiscUtils {


    /**
     * This method splits an identifier into components
     * AnnouncementAdd01 => {Announcement, Add, 01}
     * EMPTY_STRING => {EMPTY, STRING}
     * @param str
     * @return
     */
    public static List<String> splitIdentifier (String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        List<String> words = new ArrayList<String>();
        char firstChar = str.charAt(0);
        String word = Character.isLetter(firstChar)? str.substring(0, 1) : EMPTY_STRING;
        for (int i = 1; i < str.length();i++) {
            char ch = str.charAt(i);
            if (Character.isLetter(ch)) {
                if (Character.isUpperCase(ch)) {
                    if (!word.isEmpty()) {
                        words.add(word);
                    }
                    word = EMPTY_STRING;
                }
                word = word + ch;
            }
            else if (Character.isDigit(ch)) {
                if (word.isEmpty() || Character.isDigit(word.charAt(0))) {
                    word = word + ch;
                }
                else {
                    if (!word.isEmpty()) {
                        words.add(word);
                    }
                    word = EMPTY_STRING + ch;
                }
            }
            else {
                if (!word.isEmpty()) {
                    words.add(word);
                }
                word = EMPTY_STRING;
            }
        }
        if (!word.isEmpty()) {
            words.add(word);
        }

        return words;
    }

    public static <T> List<T> toList(T[] array) {
        ArrayList<T> arrayList = new ArrayList<T>(array.length);
        for (T item: array) {
            arrayList.add(item);
        }
        return arrayList;
    }

    public static <T> List<T> toList(Iterable<T> iterable) {
        ArrayList<T> arrayList = new ArrayList<T>();
        for (T item: iterable) {
            arrayList.add(item);
        }
        return arrayList;
    }

    public static <T> List<T> cloneCollection(Collection<T> collection) {
        List<T> list = new ArrayList<T>(collection.size());
        list.addAll(collection);
        return list;
    }

    public static String quoteObject(Object obj) {
        final String quote = "\"";
        final String escapedQuote = "\\\"";
        if (obj == null) {
            return quote + "null" + quote;
        }

        String str = obj.toString();
        str = quote + str.replace(quote, escapedQuote) + quote;
        return str;
    }

    private static final String HEXES = "0123456789ABCDEF";
    public static String getHexString(byte [] raw) {
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

    public static String capitalizeFirstLetter(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    public static String decapitalizeFirstLetter(String str) {
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }

    public static String removeIfStartsWith(String str, String prefix) {
        if (str.startsWith(prefix)) {
            str = str.substring(prefix.length());
        }
        return str;
    }

    public static String[] stringArrayWithValue(String value, int size) {
        String[] strings = new String[size];
        for (int i = 0; i < size; i++) {
            strings[i] = value;
        }
        return strings;
    }

    public static String buildSprocCallSql (String sprocName, int parameterCount) {
        return buildSprocCallSql(sprocName, parameterCount, true);
    }

    public static String buildSprocCallSql (String sprocName, int parameterCount, boolean hasReturnValue) {
        String sql = "{ ";
        if (hasReturnValue) {
            sql += "? = ";
        }

        sql += "call " + sprocName +
                " (" + StringUtils.join(stringArrayWithValue("?", parameterCount), ",") + ")" +
                " }";
        return sql;
    }

    public static java.sql.Date utilDateToSqlDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    public static Class<?> getCorrespondingPrimitiveType(Class<?> javaLangType) {
        try {
            if (javaLangType.isPrimitive()) {
                return javaLangType;
            }

            if (javaLangType.isArray()) {
                Class<?> primitiveComponentType = getCorrespondingPrimitiveType(javaLangType.getComponentType());
                if (primitiveComponentType == null) {
                    return null;
                }
                return Array.newInstance(primitiveComponentType, 0).getClass();
            }
            return (Class<?>)javaLangType.getDeclaredField("TYPE").get(null);
        } catch (NoSuchFieldException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    public static long now() {
        return new java.util.Date().getTime();
    }

    public static List<String> getResultSetColumnNames (ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int countOfColumns = metaData.getColumnCount();
        List<String> columnNames = new ArrayList<String>(countOfColumns);
        for (int i = 1; i <= countOfColumns; i++) {
            columnNames.add(metaData.getColumnName(i));
        }

        return columnNames;
    }

    public static List<String> getFieldNames (Class<?> type) {
        return toList(CollectionUtils.collect(toList(getApplicableFields(type)), new Transformer<Field, String>() {
                        @Override
                        public String transform(Field field) {
                            return field.getName();
                        }
                    })
                );
    }

    public static List<String> getFieldNames (Collection<Field> fields) {
        return toList(CollectionUtils.collect(fields, new Transformer<Field, String>() {
                        @Override
                        public String transform(Field field) {
                            return field.getName();
                        }
                    })
                );
    }

    public static Field[] getApplicableFields(Class<?> type) {
        boolean ignoreInheritedFields = type.isAnnotationPresent(IgnoreInheritedFields.class);
        Field[] fields = ignoreInheritedFields? type.getDeclaredFields() : type.getFields();
        List<Field> instanceFields = new ArrayList<Field>();
        for (Field field: fields) {
            if (!isStaticField(field)) {
                instanceFields.add(field);
            }
        }
        Field[] instanceFieldArray = new Field[instanceFields.size()];
        return instanceFields.toArray(instanceFieldArray);
    }

    public static boolean isStaticField(Field field) {
        return Modifier.isStatic(field.getModifiers());
    }


}

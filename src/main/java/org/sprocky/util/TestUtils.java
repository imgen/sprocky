package org.sprocky.util;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Author: Imgen
 */
public class TestUtils {
    public static <T> void assertSequenceEquals(Iterable<T> seq1, Iterable<T> seq2) {
        Iterator<T> iterator1 = seq1.iterator(),
                    iterator2 = seq2.iterator();
        boolean seqEqual = iterator1.hasNext() == iterator2.hasNext();
        while (seqEqual && iterator1.hasNext() && iterator2.hasNext()) {
            seqEqual = iterator1.next().equals(iterator2.next());
        }

        assertTrue("The two sequence should be equal", seqEqual);
    }
}

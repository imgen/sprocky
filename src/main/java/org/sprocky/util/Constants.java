package org.sprocky.util;

/**
 * Author: Imgen
 */
public class Constants {
    public static final String EMPTY_STRING = "";
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String FIELD_NAME_PREFIX = "m_";
    public static final String SQL_PARAMETER_NAME_PREFIX = "p";
}

package org.sprocky.util;

import org.sprocky.SprocParameterMapping;
import org.sprocky.TypeMapper;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Author: Imgen
 */
public class SprocHelper {
    public static void prepareSprocParameters(CallableStatement callableStatement,
                                              TypeMapper typeMapper,
                                             List<SprocParameterMapping> mappings) throws SQLException {
        for (SprocParameterMapping mapping: mappings) {
            typeMapper.setSprocParameter(callableStatement, mapping);
        }
    }
}

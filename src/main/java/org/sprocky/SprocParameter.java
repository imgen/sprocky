package org.sprocky;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author: Imgen
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SprocParameter {
    SQLDataType sqlType() default SQLDataType.None;
    String name() default "";
    SprocParameterDirection direction() default SprocParameterDirection.Input;
}

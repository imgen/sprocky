package org.sprocky;

/**
 * Author: Imgen
 */
public enum SprocParameterDirection {
    Input,
    Output,
    InputOutput
}

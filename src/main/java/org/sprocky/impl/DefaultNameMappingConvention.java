package org.sprocky.impl;

import org.sprocky.NameMappingConvention;

import java.lang.reflect.Field;

import static org.sprocky.util.MiscUtils.capitalizeFirstLetter;

/**
 * Author: Imgen
 */
public class DefaultNameMappingConvention implements NameMappingConvention {
    @Override
    public String fromClassToSproc(Class<?> type) {
        return type.getSimpleName();
    }

    @Override
    public String fromFieldToSprocParameter(Field field) {
        return capitalizeFirstLetter(field.getName());
    }

    @Override
    public String fromFieldToResultSetColumn(Field field) {
        return capitalizeFirstLetter(field.getName());
    }
}

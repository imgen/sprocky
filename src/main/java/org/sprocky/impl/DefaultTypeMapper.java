package org.sprocky.impl;

import org.apache.commons.lang3.ArrayUtils;
import org.sprocky.AbstractTypeMapper;
import org.sprocky.SQLDataType;
import org.sprocky.SprocParameterMapping;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import static org.sprocky.util.MiscUtils.capitalizeFirstLetter;
import static org.sprocky.util.MiscUtils.getCorrespondingPrimitiveType;

/**
 * Author: Imgen
 */
public class DefaultTypeMapper extends AbstractTypeMapper {
    static {
        javaToSqlTypeMap.put(Long.class, SQLDataType.BigInt);
        javaToSqlTypeMap.put(long.class, SQLDataType.BigInt);

        javaToSqlTypeMap.put(Integer.class, SQLDataType.Int);
        javaToSqlTypeMap.put(int.class, SQLDataType.Int);

        javaToSqlTypeMap.put(Short.class, SQLDataType.SmallInt);
        javaToSqlTypeMap.put(short.class, SQLDataType.SmallInt);

        javaToSqlTypeMap.put(Byte.class, SQLDataType.TinyInt);
        javaToSqlTypeMap.put(byte.class, SQLDataType.TinyInt);

        javaToSqlTypeMap.put(Float.class, SQLDataType.Float);
        javaToSqlTypeMap.put(float.class, SQLDataType.Float);

        javaToSqlTypeMap.put(Double.class, SQLDataType.Double);
        javaToSqlTypeMap.put(double.class, SQLDataType.Double);

        javaToSqlTypeMap.put(String.class, SQLDataType.Varchar);

        javaToSqlTypeMap.put(Boolean.class, SQLDataType.Bit);
        javaToSqlTypeMap.put(boolean.class, SQLDataType.Bit);

        javaToSqlTypeMap.put(Date.class, SQLDataType.Date);
        javaToSqlTypeMap.put(java.util.Date.class, SQLDataType.Date);
        javaToSqlTypeMap.put(Timestamp.class, SQLDataType.Timestamp);
        javaToSqlTypeMap.put(Time.class, SQLDataType.Time);

        javaToSqlTypeMap.put(byte[].class, SQLDataType.Varbinary);
        javaToSqlTypeMap.put(Byte[].class, SQLDataType.Varbinary);
    }

    @Override
    protected void setInputParameter(CallableStatement callableStatement, 
                                     SprocParameterMapping parameterMapping) throws Throwable {
        Class<?> fieldType = parameterMapping.fieldType;
        Object fieldValue = parameterMapping.fieldValue;
        SQLDataType sqlType = parameterMapping.sqlType;
        if (fieldType == java.util.Date.class) {
            fieldType = java.sql.Date.class;
            long time = ((java.util.Date)fieldValue).getTime();
            if (sqlType == SQLDataType.Date) {
                fieldValue = new java.sql.Date(time);
            } else if (sqlType == SQLDataType.Time) {
                fieldValue = new Time(time);
            } else if (sqlType == SQLDataType.Timestamp) {
                fieldValue = new Timestamp(time);
            }
        }
        if (fieldType == Byte[].class) {
            fieldValue = ArrayUtils.toPrimitive((Byte[]) fieldValue);
        }
        Class<?> correspondingPrimitiveType = getCorrespondingPrimitiveType(fieldType);
        if (correspondingPrimitiveType != null) {
            fieldType = correspondingPrimitiveType;
        }
        String typeName = fieldType.getSimpleName();
        String methodName;
        Class<?> secondParameterType;
        Object secondParameterValue;
        if (fieldValue != null) {
            methodName = sqlType == SQLDataType.NVarchar? "setNString" :
                                    fieldType == byte[].class || fieldType == Byte[].class?
                                    "setBytes" : "set" + capitalizeFirstLetter(typeName);
            secondParameterType = correspondingPrimitiveType != null? correspondingPrimitiveType : fieldType;
            secondParameterValue = fieldValue;
        }
        else {
            methodName = "setNull";
            secondParameterType = int.class;
            secondParameterValue = parameterMapping.sqlType.getTypeId();
        }
        logMappingInfo(methodName, fieldValue, parameterMapping);
        Method method = CallableStatement.class.getMethod(methodName, String.class, secondParameterType);
        method.invoke(callableStatement, parameterMapping.parameterName, secondParameterValue);
    }
}

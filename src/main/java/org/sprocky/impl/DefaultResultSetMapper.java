package org.sprocky.impl;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprocky.*;
import org.sprocky.exception.SprocResultSetMappingException;
import org.sprocky.util.Constants;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

import static org.sprocky.util.Constants.NEW_LINE;
import static org.sprocky.util.MiscUtils.*;

/**
 * Author: Imgen
 * TODO: Add log for result set mapping
 */
public class DefaultResultSetMapper implements ResultSetMapper {
    private static Logger logger = LoggerFactory.getLogger(DefaultResultSetMapper.class);

    private static Map<Class<?>, Map<String, Collection<Field>>> columnNameToFieldMapCache =
                                        new HashMap<Class<?>, Map<String, Collection<Field>>>();

    public <T> List<T> map(Class<T> type,
                                  ResultSet resultSet,
                                  NameMappingConvention nameMappingConvention,
                                  TypeMapper typeMapper)
            throws Throwable {
        List<T> items = new ArrayList<T>();
        Map<String, Collection<Field>> columnNameToFieldMap = getColumnNameToFieldMap(type, nameMappingConvention, typeMapper);
        List<String> columnNames = getResultSetColumnNames(resultSet);

        while (resultSet.next()) {
            Constructor<T> constructor = type.getConstructor();
            constructor.setAccessible(true);
            T item = constructor.newInstance();
            for(String mappedColumnName: columnNameToFieldMap.keySet()) {
                Collection<Field> mappedFields = columnNameToFieldMap.get(mappedColumnName);
                if (!columnNames.contains(mappedColumnName)) {
                    // We log an error about the failure, and throw the exception
                    logResultSetColumnMappingFailureAndThrow(mappedColumnName, type, mappedFields);
                }
                Object columnValue = resultSet.getObject(mappedColumnName);

                for (Field field: mappedFields) {
                    Object value = columnValue;
                    if (value != null) {
                        Class<?> fieldType = field.getType();
                        Class<?> columnType = value.getClass();

                        // For certain types, we need to do some processing
                        if (fieldType == java.util.Date.class &&
                            (columnType == java.sql.Date.class ||
                             columnType == Time.class ||
                             columnType == Timestamp.class) ) {
                            // We will handle this type incompatibility and java stupidity gracefully
                            Method method = columnType.getMethod("getTime");
                            Long time = (Long)method.invoke(columnValue);
                            value = new Date(time);
                        }
                        if (fieldType == Byte[].class &&
                            columnType == byte[].class) {
                            // Another mismatch and java stupidity, we can handle it
                            value = ArrayUtils.toObject((byte[])value);
                        }
                    }
                    try {
                        field.setAccessible(true);
                        field.set(item, value);
                    } catch (Throwable e) {
                        logResultSetColumnMappingFailureAndThrow(mappedColumnName, type, field, e);
                    }
                }
            }

            items.add(item);
        }

        return items;
    }

    private static Map<String, Collection<Field>> getColumnNameToFieldMap(Class<?> type,
                                                                          NameMappingConvention nameMappingConvention,
                                                                          TypeMapper typeMapper) {
        if (columnNameToFieldMapCache.containsKey(type)) {
            return columnNameToFieldMapCache.get(type);
        }
        Map<String, Collection<Field>> columnNameToFieldMap = buildColumnNameToFieldMap(type, nameMappingConvention, typeMapper);
        columnNameToFieldMapCache.put(type, columnNameToFieldMap);
        logColumnNameToFieldMapInfo(type, columnNameToFieldMap);
        return columnNameToFieldMap;
    }

    private static Map<String, Collection<Field>> buildColumnNameToFieldMap(Class<?> type,
                                                                            NameMappingConvention nameMappingConvention,
                                                                            TypeMapper typeMapper) {
        Field[] fields = getApplicableFields(type);
        Map<String, Collection<Field>> columnNameToFieldMap = new HashMap<String, Collection<Field>>(fields.length);
        for (Field field: fields) {
            String columnName;
            if (field.isAnnotationPresent(ResultSetColumnIgnore.class)) {
                // We log the ignored field, then continue
                logIgnoredField(field, "it is marked with @ResultSetColumnIgnore", false);
                if (field.isAnnotationPresent(ResultSetColumn.class)) {
                    logger.warn("@ResultSetColumnIgnore annotation overrides @ResultSetColumn annotation " +
                            "on the field " + field.getName() +
                            " of type " + field.getDeclaringClass().getName());
                }
                continue;
            }
            Class<?> fieldType = field.getType();
            SQLDataType sqlType = typeMapper.fromJavaToSqlType(fieldType);
            if (sqlType == null || sqlType == SQLDataType.None) {
                // We log the ignored field, then continue
                logIgnoredField(field, "the field's type " + fieldType.getName() +
                                " cannot be mapped to a proper SQL type using current TypeMapper implementation", true);
                continue;
            }
            if (field.isAnnotationPresent(ResultSetColumn.class)) {
                ResultSetColumn resultSetColumn = field.getAnnotation(ResultSetColumn.class);
                columnName = resultSetColumn.name();
            } else {
                columnName = nameMappingConvention.fromFieldToResultSetColumn(field);
            }
            if (!columnNameToFieldMapCache.containsKey(columnName)) {
                columnNameToFieldMap.put(columnName, new ArrayList<Field>());
            }
            columnNameToFieldMap.get(columnName).add(field);
        }
        return columnNameToFieldMap;
    }

    private static void logColumnNameToFieldMapInfo(final Class<?> type,
                                                    final Map<String,
                                                    Collection<Field>> columnNameToFieldMap) {
        if (columnNameToFieldMap.size() == 0) {
            return;
        }
        String message = "For type " + type.getName() + ", the result set column to field mapping is below: " + NEW_LINE;

        Collection<String> mappingInfo = CollectionUtils.collect(columnNameToFieldMap.keySet(),
                new Transformer<String, String>() {
                    @Override
                    public String transform(String columnName) {
                        Collection<Field> fields = columnNameToFieldMap.get(columnName);
                        Collection<String> fieldNames = getFieldNames(fields);
                        return columnName + " => " + StringUtils.join(fieldNames, ", ");
                    }
                });
        message += StringUtils.join(mappingInfo, NEW_LINE);
        logger.info(message);
    }

    private static void logIgnoredField(Field field, String reason, boolean isWarning) {
        String message = "The field " + field.getName() + " of type " + field.getDeclaringClass().getName() +
                " is ignored in result set mapping process due to the fact that " +
                reason;
        if (isWarning) {
            logger.warn(message);
        }
        else {
            logger.info(message);
        }
    }

    private static void logResultSetColumnMappingFailureAndThrow(String mappedColumnName,
                                                                 Class<?> type,
                                                                 Collection<Field> fields) {
        Collection<String> fieldNames = getFieldNames(fields);
        String message = "Cannot find the assumed column " + mappedColumnName +
                " which is mapped from following fields of type " + type.getName() +
                ": " + Constants.NEW_LINE + StringUtils.join(fieldNames, Constants.NEW_LINE);
        logger.error(message);
        throw new SprocResultSetMappingException(message, null);
    }

    private static void logResultSetColumnMappingFailureAndThrow(String mappedColumnName,
                                                                 Class<?> type,
                                                                 Field field,
                                                                 Throwable cause) {
        String message = "Cannot find the assumed column " + mappedColumnName +
                " which is mapped from the field " + field.getName() +
                " of type " + type.getName();
        logger.error(message);
        throw new SprocResultSetMappingException(message, cause);
    }
}

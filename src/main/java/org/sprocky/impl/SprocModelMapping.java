package org.sprocky.impl;

import org.sprocky.SprocParameterMapping;

import java.util.Collection;

/**
 * Author: Imgen
 */
public class SprocModelMapping {
    public String sprocName;
    public Collection<SprocParameterMapping> parameters;
}

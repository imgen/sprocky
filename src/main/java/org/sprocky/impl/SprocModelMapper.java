package org.sprocky.impl;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprocky.*;
import org.sprocky.exception.SprocParameterMappingException;

import java.lang.reflect.Field;
import java.util.*;

import static org.sprocky.util.Constants.EMPTY_STRING;
import static org.sprocky.util.Constants.NEW_LINE;
import static org.sprocky.util.MiscUtils.getApplicableFields;

/**
 * Author: Imgen
 */
public class SprocModelMapper {
    private static Logger logger = LoggerFactory.getLogger(SprocModelMapper.class);
    private static Map<Class<?>, SprocModelMapping> sprocModelMappingCache = new HashMap<Class<?>, SprocModelMapping>();

    public static SprocModelMapping map(Object model,
                                        NameMappingConvention nameMappingConvention) {
        return map(model, nameMappingConvention, null);
    }

    public static SprocModelMapping map(Object model,
                                        NameMappingConvention nameMappingConvention,
                                        TypeMapper typeMapper) {
        logger.trace("Processing sproc model");
        if (typeMapper == null) {
            typeMapper = new DefaultTypeMapper();
        }
        SprocModelMapping sprocModelMapping = getSprocModelMapping(model, nameMappingConvention, typeMapper);

        return sprocModelMapping;
    }

    private static SprocModelMapping getSprocModelMapping(Object model,
                                                          NameMappingConvention nameMappingConvention,
                                                          TypeMapper typeMapper) {
        Class<?> type = model.getClass();
        SprocModelMapping sprocModelMapping;
        if (sprocModelMappingCache.containsKey(type)) {
            sprocModelMapping = sprocModelMappingCache.get(type);
        }
        else {
            sprocModelMapping = new SprocModelMapping();
            sprocModelMapping.sprocName = getSprocName(nameMappingConvention, type);
            sprocModelMapping.parameters = getSprocParameterMapping(type, nameMappingConvention, typeMapper);
            sprocModelMappingCache.put(type, sprocModelMapping);
            logMappingInfo(model.getClass(), sprocModelMapping);
        }

        // Set the fieldValue of each SprocParameterMapping
        for (SprocParameterMapping parameterMapping: sprocModelMapping.parameters) {
            Field field = parameterMapping.field;
            try {
                field.setAccessible(true);
                parameterMapping.fieldValue = field.get(model);
            } catch (IllegalAccessException e) {
                // We swallow it
            }
        }

        return sprocModelMapping;
    }

    private static void logMappingInfo(Class<?> type, SprocModelMapping sprocModelMapping) {
        String message = "For type " + type.getName() +
                         " the model mapping info is as below: " + NEW_LINE +
                         type.getSimpleName() + " -> " + sprocModelMapping.sprocName;
        for (SprocParameterMapping parameterMapping: sprocModelMapping.parameters) {
            message += NEW_LINE + parameterMapping.field.getName() + " => " + parameterMapping.parameterName;
        }
        logger.info(message);
    }

    private static Collection<SprocParameterMapping> getSprocParameterMapping(Class<?> type,
                                                                              NameMappingConvention nameMappingConvention,
                                                                              TypeMapper typeMapper) {
        Field[] fields = getApplicableFields(type);
        List<SprocParameterMapping> parameterMappings = new ArrayList<SprocParameterMapping>();
        for (Field field: fields) {
            if (field.isAnnotationPresent(SprocParameterIgnore.class)) {
                // Ignore the field marked with SprocParameterIgnore
                logIgnoredField(field, "it is marked with @SprocParameterIgnore annotation", false);
                if (field.isAnnotationPresent(SprocParameter.class)) {
                    logger.warn("@SprocParameterIgnore annotation overrides @SprocParameter annotation " +
                            "on the field " + field.getName() +
                            " of type " + field.getDeclaringClass().getName());
                }
                continue;
            }
            SprocParameterMapping parameterMapping = new SprocParameterMapping();
            parameterMapping.field = field;
            parameterMapping.parameterName = nameMappingConvention.fromFieldToSprocParameter(field);
            Class<?> fieldType = field.getType();
            parameterMapping.fieldType = fieldType;
            SQLDataType mappedSqlType = typeMapper.fromJavaToSqlType(fieldType);
            if (mappedSqlType == null || mappedSqlType == SQLDataType.None) {
                // No mapped sql type, we log it as an warning and continue
                logIgnoredField(field,
                                "sprocky cannot map the field's type to a proper SQL type " +
                                "using current TypeMapper implementation",
                                true);
                continue;
            }
            parameterMapping.sqlType = mappedSqlType;


            if (field.isAnnotationPresent(SprocParameter.class)) {
                SprocParameter sprocParameter = field.getAnnotation(SprocParameter.class);

                if (!sprocParameter.name().equals(EMPTY_STRING)) {
                    parameterMapping.parameterName = sprocParameter.name();
                }
                if (!sprocParameter.sqlType().equals(SQLDataType.None)) {
                    parameterMapping.sqlType = sprocParameter.sqlType();
                }
                parameterMapping.direction = sprocParameter.direction();
            }

            checkDuplicateParameterMapping(parameterMappings, parameterMapping);
            parameterMappings.add(parameterMapping);
        }

        return parameterMappings;
    }

    private static void checkDuplicateParameterMapping(List<SprocParameterMapping> parameterMappings,
                                                       final SprocParameterMapping parameterMapping) {
        SprocParameterMapping foundParameterMapping = CollectionUtils.find(parameterMappings,
                new Predicate<SprocParameterMapping>() {
                    @Override
                    public boolean evaluate(SprocParameterMapping parameterMapping2) {
                        return parameterMapping2.parameterName.equals(parameterMapping.parameterName);
                    }
                });
        if (foundParameterMapping != null) {
            Field field = parameterMapping.field;
            String message = "Cannot map the field " + field.getName() + " of type " +
                             field.getDeclaringClass().getName() + " to the desired parameter with name " +
                             parameterMapping.parameterName + " due to the fact that another field " +
                             foundParameterMapping.field.getName() + " is already mapped to it";
            logger.error(message);
            throw new SprocParameterMappingException(message, parameterMapping);
        }
    }

    private static String getSprocName(NameMappingConvention nameMappingConvention, Class<?> type) {
        String name = nameMappingConvention.fromClassToSproc(type);
        if (type.isAnnotationPresent(SprocModel.class)) {
            SprocModel sprocModel = type.getAnnotation(SprocModel.class);
            name = sprocModel.name();
        }
        return name;
    }

    private static void logIgnoredField(Field field, String reason, boolean isWarning) {
        String message = "The field " + field.getName() + " of type " + field.getDeclaringClass().getName() +
                " is ignored in sproc model mapping process due to the fact that " + reason;
        if (isWarning) {
            logger.warn(message);
        } else {
            logger.info(message);
        }
    }
}

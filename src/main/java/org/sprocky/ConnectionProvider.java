package org.sprocky;

import org.sprocky.exception.DataAccessException;

import java.sql.Connection;

/**
 * Author: Imgen
 */
public interface ConnectionProvider {

    /**
     * Acquire a connection from the connection lifecycle handler
     * <p>
     * The general contract is that a <code>ConnectionProvider</code> is
     * expected to always return the same connection, until this connection is
     * returned by sprocky through {@link #release(Connection)}.
     *
     * @throws DataAccessException If anything went wrong while acquiring a
     *             connection
     */
    Connection acquire() throws DataAccessException;

    /**
     * Release a connection to the connection lifecycle handler
     * <p>
     * The general contract is that a <code>ConnectionProvider</code> must not
     * generate a new connection in {@link #acquire()}, before a previous
     * connection is returned.
     *
     * @throws DataAccessException If anything went wrong while releasing a
     *             connection
     */
    void release(Connection connection) throws DataAccessException;
}

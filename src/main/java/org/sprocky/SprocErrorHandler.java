package org.sprocky;

/**
 * Author: Imgen
 */
public interface SprocErrorHandler {
    void handle(Throwable exception) throws Throwable;
}

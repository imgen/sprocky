package org.sprocky;

import java.sql.CallableStatement;

/**
 * Author: Imgen
 */
public interface TypeMapper {
    SQLDataType fromJavaToSqlType(Class<?> type);
    void setSprocParameter(CallableStatement callableStatement,
                           SprocParameterMapping parameterMapping);
    void retrieveOutputParameter(CallableStatement callableStatement,
                                 Object sprocModel,
                                 SprocParameterMapping parameterMapping) throws Throwable;
}

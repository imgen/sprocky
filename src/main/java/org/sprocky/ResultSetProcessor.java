package org.sprocky;

import java.sql.ResultSet;

/**
 * Author: Imgen
 * This interface is used for supporting result set of a sproc. An user of the sprocky API could
 * implement this API to process the returned ResultSet to avoid creating a POJO class to map the
 * ResultSet
 */
public interface ResultSetProcessor {
    void process(ResultSet resultSet) throws Throwable;
}

package org.sprocky;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprocky.impl.DefaultResultSetMapper;
import org.sprocky.impl.DefaultTypeMapper;
import org.sprocky.impl.SprocModelMapper;
import org.sprocky.impl.SprocModelMapping;
import org.sprocky.util.MiscUtils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Imgen
 * This is the core of the API. This class is responsible for holding the configuration and execute
 * sproc(s) over it. The configuration may need to be in a separate class, but for now it is OK to be
 * in one
 *
 */
public class SprocProfile {
    private static Logger logger = LoggerFactory.getLogger(SprocProfile.class);

    public SprocProfile(String name) {
        this.name = name;
    }

    private String name;
    public String getName () {
        return this.name;
    }

    //<editor-fold desc="Configuration">
    private TypeMapper typeMapper = new DefaultTypeMapper();
    public void setTypeMapper(TypeMapper typeMapper) {
        this.typeMapper = typeMapper;
    }

    private ResultSetMapper resultSetMapper = new DefaultResultSetMapper();
    public ResultSetMapper getResultSetMapper() {
        return this.resultSetMapper;
    }
    public void setResultSetMapper(ResultSetMapper resultSetMapper) {
        this.resultSetMapper = resultSetMapper;
    }

    private NameMappingConvention nameMappingConvention;
    public void setNameMappingConvention(NameMappingConvention nameMappingConvention) {
        this.nameMappingConvention = nameMappingConvention;
    }

    private ConnectionProvider connectionProvider;
    public void setConnectionProvider(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    private SQLDialect dialect;
    public void setDialect(SQLDialect dialect) {
        this.dialect = dialect;
    }

    private SprocErrorHandler errorHandler;
    public void setErrorHandler(SprocErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    //</editor-fold>


    //<editor-fold desc="NonQuery Sproc executors">
    public Integer executeNonQuery(final String sprocName) throws Throwable {
        return executeNonQuery(sprocName, null);
    }

    public Integer executeNonQuery(final String sprocName, Object singleParameter) throws Throwable {
        AbstractSprocActor sprocActor = new SprocActorWithSprocName(sprocName, singleParameter) {};
        return executeAndRetrieveReturnValue(sprocActor);
    }

    public <TSprocModel> Integer executeNonQuery(TSprocModel sprocModel) throws Throwable {
        SprocActorWithModel sprocActor = new SprocActorWithModel(sprocModel) {};
        return executeAndRetrieveReturnValue(sprocActor);
    }

    //</editor-fold>


    //<editor-fold desc="Scalar Sproc executors">
    public ScalarSprocResult executeScalar(String sprocName) throws Throwable {
        return executeScalar(sprocName, null);
    }

    public ScalarSprocResult executeScalar(String sprocName, Object singleParameter) throws Throwable {
        SprocActorWithSprocNameAndScalarValue sprocActor =
                new SprocActorWithSprocNameAndScalarValue(sprocName, singleParameter);
        stageSprocActor(sprocActor);
        return sprocActor.getScalarResult();
    }

    public <TSprocModel> ScalarSprocResult executeScalar(TSprocModel sprocModel) throws Throwable {
        SprocActorWithModelAndScalarResult sprocActor = new SprocActorWithModelAndScalarResult(sprocModel);
        stageSprocActor(sprocActor);
        return sprocActor.getScalarResult();
    }


    //</editor-fold>


    //<editor-fold desc="Query Sproc executors">
    public <TResult> QuerySprocResult<TResult> executeQuery(String sprocName,
                                                            Class<TResult> resultType) throws Throwable {
        return executeQuery(sprocName, null, resultType);
    }

    public QuerySprocResult executeQuery(String sprocName,
                                         Class<?>[] resultTypes) throws Throwable {
        return executeQuery(sprocName, null, resultTypes);
    }

    public Integer executeQuery(String sprocName, final ResultSetProcessor resultSetProcessor) throws Throwable {
        return executeQuery(sprocName, null, resultSetProcessor);
    }

    public Integer executeQuery(String sprocName,
                                final MultiResultSetProcessor resultSetProcessor) throws Throwable {
        return executeQuery(sprocName, null, resultSetProcessor);
    }

    public <TResult> QuerySprocResult<TResult> executeQuery(String sprocName,
                                                           Object singleParameter,
                                                           Class<TResult> resultType) throws Throwable {
        SprocActorWithSprocNameAndResultType<TResult> sprocActor =
                new SprocActorWithSprocNameAndResultType<TResult>(sprocName, singleParameter, resultType);
        stageSprocActor(sprocActor);
        return sprocActor.getQueryResult();
    }

    public QuerySprocResult<?> executeQuery(String sprocName,
                                             Object singleParameter,
                                             Class<?>[] resultTypes) throws Throwable {
        SprocActorWithSprocNameAndResultTypes sprocActor =
                new SprocActorWithSprocNameAndResultTypes(sprocName, singleParameter, resultTypes);
        stageSprocActor(sprocActor);
        return sprocActor.getQueryResult();
    }

    public Integer executeQuery(String sprocName, Object singleParameter, final ResultSetProcessor resultSetProcessor) throws Throwable {
        AbstractSprocActor sprocActor = new SprocActorWithSprocName(sprocName, null) {
            @Override
            public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
                if (resultSetIndex == 0) {
                    resultSetProcessor.process(resultSet);
                }
            }
        };

        return executeAndRetrieveReturnValue(sprocActor);
    }

    public Integer executeQuery(String sprocName,
                                Object singleParameter,
                                final MultiResultSetProcessor multiResultSetProcessor) throws Throwable {
        AbstractSprocActor sprocActor = new SprocActorWithSprocName(sprocName, singleParameter) {
            @Override
            public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
                multiResultSetProcessor.process(resultSet, resultSetIndex);
            }
        };

        return executeAndRetrieveReturnValue(sprocActor);
    }

    public <TResult> QuerySprocResult<TResult> executeQuery(Object sprocModel,
                                                            Class<TResult> resultType) throws Throwable {
        SprocActorWithModelAndResultType<TResult> sprocActor =
                new SprocActorWithModelAndResultType<TResult>(sprocModel, resultType);
        stageSprocActor(sprocActor);
        return sprocActor.getQueryResult();
    }

    public <TSprocModel> QuerySprocResult executeQuery(TSprocModel sprocModel,
                                                       Class<?>[] resultTypes) throws Throwable {
        SprocActorWithModelAndResultTypes sprocActor = new SprocActorWithModelAndResultTypes(sprocModel, resultTypes);
        stageSprocActor(sprocActor);
        return sprocActor.getQueryResult();
    }

    public <TSprocModel> Integer executeQuery(TSprocModel sprocModel,
                                              final ResultSetProcessor resultSetProcessor) throws Throwable {
        AbstractSprocActor sprocActor = new SprocActorWithModel(sprocModel) {
            @Override
            public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
                if (resultSetIndex == 0) {
                    resultSetProcessor.process(resultSet);
                }
            }
        };
        return executeAndRetrieveReturnValue(sprocActor);
    }

    public <TSprocModel> Integer executeQuery(TSprocModel sprocModel,
                                              final MultiResultSetProcessor multiResultSetProcessor) throws Throwable {
        AbstractSprocActor sprocActor = new SprocActorWithModel(sprocModel) {
            @Override
            public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
                multiResultSetProcessor.process(resultSet, resultSetIndex);
            }
        };
        return executeAndRetrieveReturnValue(sprocActor);
    }


    //</editor-fold>


    //<editor-fold desc="SprocActors">
    private interface SprocActor {
        String prepareSql();
        void setParameters(CallableStatement callableStatement) throws Throwable;
        void processOutParameters(CallableStatement callableStatement) throws Throwable;
        void processReturnValue(CallableStatement callableStatement) throws SQLException;
        void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable;
    }

    private abstract class AbstractSprocActor implements SprocActor{
        public int returnValue;

        @Override
        public void setParameters(CallableStatement callableStatement) throws Throwable {
        }

        @Override
        public void processOutParameters(CallableStatement callableStatement) throws Throwable {
        }

        @Override
        public void processReturnValue(CallableStatement callableStatement) throws SQLException {
            this.returnValue = callableStatement.getInt(1);
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
        }
    }

    private abstract class SprocActorWithSprocName extends AbstractSprocActor {
        private String sprocName;
        private Object singleParameter;
        public SprocActorWithSprocName(String sprocName, Object singleParameter) {
            this.sprocName = sprocName;
            this.singleParameter = singleParameter;
        }

        @Override
        public String prepareSql() {
            int parameterCount = singleParameter == null? 0 : 1;
            return MiscUtils.buildSprocCallSql(sprocName, parameterCount);
        }

        @Override
        public void setParameters(CallableStatement callableStatement) throws Throwable {
            if (singleParameter == null) {
                return;
            }
            Class<?> type = singleParameter.getClass();
            final SQLDataType sqlDataType = typeMapper.fromJavaToSqlType(type);
            if (type == java.util.Date.class) {
                singleParameter = MiscUtils.utilDateToSqlDate((java.util.Date)singleParameter);
            }
            if (type == Byte[].class) {
                singleParameter = ArrayUtils.toPrimitive((Byte[]) singleParameter);
            }

            callableStatement.setObject(1, singleParameter, sqlDataType.getTypeId());
        }
    }

    private class SprocActorWithSprocNameAndScalarValue extends SprocActorWithSprocName {
        private Object scalarValue;

        public SprocActorWithSprocNameAndScalarValue(String sprocName, Object singleParameter) {
            super(sprocName, singleParameter);
        }

        public ScalarSprocResult getScalarResult() {
            return new ScalarSprocResult(this.returnValue, this.scalarValue);
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws SQLException {
            this.scalarValue = getScalarValue(resultSet, resultSetIndex);
        }
    }

    private class SprocActorWithSprocNameAndResultType<TResult> extends SprocActorWithSprocName {
        private Class<TResult> resultType;
        private List<TResult> queryResult;

        public QuerySprocResult<TResult> getQueryResult() {
            return new QuerySprocResult<TResult>(returnValue, queryResult, null);
        }

        public SprocActorWithSprocNameAndResultType(String sprocName, Object singleParameter, Class<TResult> resultType) {
            super(sprocName, singleParameter);
            this.resultType = resultType;
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
            this.queryResult = mapQueryResult(resultSet, resultSetIndex, this.resultType);
        }
    }

    private class SprocActorWithSprocNameAndResultTypes extends SprocActorWithSprocName {
        private Class<?>[] resultTypes ;
        private List<List<?>> queryResults = new ArrayList<List<?>>();
        public SprocActorWithSprocNameAndResultTypes(String sprocName, Object singleParameter, Class<?>[] resultTypes) {
            super(sprocName, singleParameter);
            this.resultTypes = resultTypes;
        }

        public QuerySprocResult<?> getQueryResult() {
            return new QuerySprocResult<Object>(this.returnValue, null, this.queryResults);
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
            this.queryResults.add(mapQueryResult(resultSet, resultSetIndex, this.resultTypes));
        }
    }

    private abstract class SprocActorWithModel extends AbstractSprocActor {
        private SprocModelMapping mapping;
        private Object model;

        public SprocActorWithModel(Object model) {
            this.mapping = SprocModelMapper.map(model, nameMappingConvention, typeMapper);
            this.model = model;
        }

        @Override
        public String prepareSql() {
            return MiscUtils.buildSprocCallSql(mapping.sprocName, mapping.parameters.size());
        }

        @Override
        public void setParameters(CallableStatement callableStatement) throws Throwable {
            for (SprocParameterMapping parameterMapping: mapping.parameters) {
                typeMapper.setSprocParameter(callableStatement, parameterMapping);
            }
        }

        @Override
        public void processOutParameters(CallableStatement callableStatement) throws Throwable {
            for (SprocParameterMapping parameterMapping: mapping.parameters) {
                typeMapper.retrieveOutputParameter(callableStatement, this.model, parameterMapping);
            }
        }
    }

    private class SprocActorWithModelAndScalarResult extends SprocActorWithModel {
        private Object scalarValue;

        public ScalarSprocResult getScalarResult() {
            return new ScalarSprocResult(this.returnValue, this.scalarValue);
        }

        public SprocActorWithModelAndScalarResult(Object model) {
            super(model);
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws SQLException {
            this.scalarValue = getScalarValue(resultSet, resultSetIndex);
        }
    }

    private class SprocActorWithModelAndResultType<TResult> extends SprocActorWithModel {
        private Class<TResult> resultType;
        private List<TResult> queryResult;

        public QuerySprocResult<TResult> getQueryResult() {
            return new QuerySprocResult<TResult>(returnValue, queryResult, null);
        }

        public SprocActorWithModelAndResultType(Object model, Class<TResult> resultType) {
            super(model);
            this.resultType = resultType;
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
            this.queryResult = mapQueryResult(resultSet, resultSetIndex, this.resultType);
        }
    }

    private class SprocActorWithModelAndResultTypes extends SprocActorWithModel {
        private Class<?>[] resultTypes;
        private List<List<?>> queryResults = new ArrayList<List<?>>();

        public QuerySprocResult<?> getQueryResult() {
            return new QuerySprocResult<Object>(returnValue, null, queryResults);
        }

        public SprocActorWithModelAndResultTypes(Object model, Class<?>[] resultTypes) {
            super(model);
            this.resultTypes = resultTypes;
        }

        @Override
        public void processResultSet(ResultSet resultSet, int resultSetIndex) throws Throwable {
            this.queryResults.add(mapQueryResult(resultSet, resultSetIndex, this.resultTypes));
        }
    }
    //</editor-fold>


    //<editor-fold desc="Helper methods">
    private static Object getScalarValue(ResultSet resultSet, int resultSetIndex) throws SQLException {
        if (resultSetIndex == 0 && resultSet.next()) {
            logger.trace("Retrieving scalar value");
            return resultSet.getObject(1);
        }

        return null;
    }

    private <T> List<T> mapQueryResult(ResultSet resultSet, int resultSetIndex, Class<T> resultType) throws Throwable {
        if (resultSetIndex == 0) {
            return resultSetMapper.map(resultType, resultSet, this.nameMappingConvention, this.typeMapper);
        }

        return null;
    }

    private List<?> mapQueryResult(ResultSet resultSet, int resultSetIndex, Class<?>[] resultTypes) throws Throwable {
        Class<?> type = resultTypes[resultSetIndex];
        return resultSetMapper.map(type, resultSet, this.nameMappingConvention, this.typeMapper);
    }

    private Integer executeAndRetrieveReturnValue(AbstractSprocActor sprocActor) throws Throwable {
        stageSprocActor(sprocActor);
        return sprocActor.returnValue;
    }

    private static void processResultSets(SprocActor sprocActor, CallableStatement callableStatement) throws Throwable {
        logger.trace("Processing resulst sets, if any");
        int resultSetIndex = 0;
        while (callableStatement.getMoreResults()) {
            ResultSet resultSet = callableStatement.getResultSet();
            try {
                logger.trace("Processing resulst set " + resultSetIndex);
                sprocActor.processResultSet(resultSet, resultSetIndex);
            } finally {
                logger.trace("Closing result set " + resultSetIndex);
                resultSet.close();
            }

            resultSetIndex++;
        }
    }
    //</editor-fold>

    /**
     * This method serves as a stage for an SprocActor to perform on during different frames
     * @param sprocActor The sproc actor to perform on the stage
     */
    private void stageSprocActor(SprocActor sprocActor) throws Throwable {
        Connection connection = null;
        try {
            logger.trace("Acquiring connection");
            connection = this.connectionProvider.acquire();
            logger.trace("Preparing sql");
            String sql = sprocActor.prepareSql();
            logger.trace("Preparing call");
            CallableStatement callableStatement = connection.prepareCall(sql);

            try {
                logger.trace("Setting parameters, if any");
                sprocActor.setParameters(callableStatement);
                logger.trace("Executing sproc on the DB");
                boolean isResultSet = callableStatement.execute();
                logger.trace("Processing output parameters, if any");
                sprocActor.processOutParameters(callableStatement);
                logger.trace("Processing return value, if there is one");
                sprocActor.processReturnValue(callableStatement);

                if (isResultSet) {
                    processResultSets(sprocActor, callableStatement);
                }
            } finally {
                logger.trace("Closing CallableStatement");
                callableStatement.close();
            }
        } catch (Throwable throwable) {
            logger.trace("Error happened during executing sproc");
            errorHandler.handle(throwable);
        } finally {
            logger.trace("Finished calling sproc");
            logger.trace("Releasing connection");
            this.connectionProvider.release(connection);
        }
    }

}

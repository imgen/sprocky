package org.sprocky;

/**
 * Author: Imgen
 */
public enum SQLDialect {
    SQLServer("SQL Server");

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private SQLDialect(String name) {
        this.name = name;
    }

}

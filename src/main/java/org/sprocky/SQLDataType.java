package org.sprocky;

import java.sql.Types;

/**
 * Author: Imgen
 */
public enum SQLDataType {
    None(Integer.MIN_VALUE),
    Null(Types.NULL),
    BigInt(Types.BIGINT),
    Int(Types.INTEGER),
    SmallInt(Types.SMALLINT),
    TinyInt(Types.TINYINT),
    Bit(Types.BIT),
    Float(Types.FLOAT),
    Double(Types.DOUBLE),
    Date(Types.DATE),
    Timestamp(Types.TIMESTAMP),
    Time(Types.TIME),
    Varchar(Types.VARCHAR),
    NVarchar(Types.NVARCHAR),
    Varbinary(Types.VARBINARY);

    private int typeId;
    private SQLDataType(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeId () {
        return this.typeId;
    }
}

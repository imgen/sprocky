package org.sprocky;

import java.sql.ResultSet;

/**
 * Author: Imgen
 * This interface is used for supporting multiple result set of a sproc. An user of the sprocky API could
 * implement this API to process the returned ResultSet(s) to avoid creating a POJO class to map the
 * ResultSet(s)
 */
public interface MultiResultSetProcessor {
    void process(ResultSet resultSet, int resultSetIndex) throws Throwable;
}

package org.sprocky;

import java.lang.reflect.Field;

/**
 * Author: Imgen
 */
public class SprocParameterMapping {
    public String parameterName;
    public Field field;
    public Class<?> fieldType;
    public Object fieldValue;
    public SQLDataType sqlType;
    public SprocParameterDirection direction = SprocParameterDirection.Input;

    public SprocParameterMapping(String parameterName,
                                 Field field,
                                 Class<?> fieldType,
                                 Object fieldValue,
                                 SQLDataType sqlType) {
        this(parameterName, field, fieldType, fieldValue, sqlType, SprocParameterDirection.Input);
    }

    public SprocParameterMapping(String parameterName,
                                 Field field,
                                 Class<?> fieldType,
                                 Object fieldValue,
                                 SQLDataType sqlType,
                                 SprocParameterDirection direction) {
        this.parameterName = parameterName;
        this.field = field;
        this.fieldType = fieldType;
        this.fieldValue = fieldValue;
        this.sqlType = sqlType;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SprocParameterMapping that = (SprocParameterMapping) o;

        if (direction != that.direction) return false;
        if (!fieldType.equals(that.fieldType)) return false;
        if (fieldValue != null ? !fieldValue.equals(that.fieldValue) : that.fieldValue != null) return false;
        if (!parameterName.equals(that.parameterName)) return false;
        if (sqlType != that.sqlType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = parameterName.hashCode();
        result = 31 * result + fieldType.hashCode();
        result = 31 * result + (fieldValue != null ? fieldValue.hashCode() : 0);
        result = 31 * result + sqlType.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }

    public SprocParameterMapping() {
    }

}

package org.sprocky;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprocky.exception.SprocParameterMappingException;
import org.sprocky.impl.DefaultTypeMapper;

import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.sprocky.util.MiscUtils.quoteObject;

/**
 * Author: Imgen
 */
public abstract class AbstractTypeMapper implements TypeMapper {
    protected static Logger logger = LoggerFactory.getLogger(DefaultTypeMapper.class);
    protected static Map<Class<?>, SQLDataType> javaToSqlTypeMap = new HashMap<Class<?>, SQLDataType>();

    @Override
    public SQLDataType fromJavaToSqlType(Class<?> type) {
        if (javaToSqlTypeMap.containsKey(type)) {
            return javaToSqlTypeMap.get(type);
        }
        return null;
    }

    @Override
    public void setSprocParameter(CallableStatement callableStatement, SprocParameterMapping parameterMapping) {
        SprocParameterDirection direction = parameterMapping.direction;
        try {
            if (direction != SprocParameterDirection.Output) {
                setInputParameter(callableStatement, parameterMapping);
            }
            if (direction != SprocParameterDirection.Input) {
                setOutputParameter(callableStatement, parameterMapping);
            }
        } catch (Throwable throwable) {
            logMappingErrorAndThrow(parameterMapping, throwable);
        }
    }

    @Override
    public void retrieveOutputParameter(CallableStatement callableStatement,
                                        Object sprocModel,
                                        SprocParameterMapping parameterMapping) throws Throwable {
        if (parameterMapping.direction == SprocParameterDirection.Input) {
            return;
        }

        Field field = parameterMapping.field;
        try {
            Object value = callableStatement.getObject(parameterMapping.parameterName);
            if (value != null) {
                Class<?> fieldType = parameterMapping.fieldType;
                Class<?> columnType = value.getClass();
                if (fieldType == java.util.Date.class &&
                    columnType == java.sql.Date.class) {
                    value = new java.util.Date(((java.sql.Date)value).getTime());
                }
                if (fieldType == Byte[].class &&
                    columnType == byte[].class) {
                    value = ArrayUtils.toObject((byte[])value);
                }
            }

            field.setAccessible(true);
            field.set(sprocModel, value);
        } catch (RuntimeException exception) {
            String message = "Cannot retrieve output parameter " + parameterMapping.parameterName +
                    " for field " + field.getName() +
                    " of type " + field.getDeclaringClass().getName();
            logger.error(message);
            throw new SprocParameterMappingException(message, exception);
        }
    }

    protected static void logMappingErrorAndThrow(SprocParameterMapping parameterMapping, Throwable e) {
        String errorMessage = "Unable to set sproc parameter with sprocName " + parameterMapping.parameterName +
                " for field " + parameterMapping.field.getName() +
                " of type " + parameterMapping.field.getDeclaringClass().getName() +
                " with value " + quoteObject(parameterMapping.fieldValue);
        logger.error(errorMessage);
        throw new SprocParameterMappingException(errorMessage, parameterMapping, e);
    }

    protected static void logMappingInfo (String methodName,
                                          Object value,
                                          SprocParameterMapping mapping) {
        logger.trace("Successfully mapped the field " + mapping.field.getName() +
                    " of type " + mapping.field.getDeclaringClass().getName() +
                    " with value " + quoteObject(value) + " by calling method " +
                    methodName + " on CallableStatement");
    }

    protected static void setOutputParameter(CallableStatement callableStatement,
                                             SprocParameterMapping parameterMapping) throws SQLException {
        callableStatement.registerOutParameter(parameterMapping.parameterName, parameterMapping.sqlType.getTypeId());
    }

    protected abstract void setInputParameter(CallableStatement callableStatement,
                                              SprocParameterMapping parameterMapping) throws Throwable;
}

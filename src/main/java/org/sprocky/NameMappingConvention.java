package org.sprocky;



import java.lang.reflect.Field;

/**
 * Author: Imgen
 * This interface defines the sprocName mapping convention between
 * POJO class and Sproc
 */
public interface NameMappingConvention {
    String fromClassToSproc(Class<?> type);
    String fromFieldToSprocParameter(Field field);
    String fromFieldToResultSetColumn(Field field);
}

package org.sprocky.exception;

import org.sprocky.SprocParameterMapping;

/**
 * Author: Imgen
 */
public class SprocParameterMappingException extends RuntimeException {
    public SprocParameterMapping parameterMapping;

    public SprocParameterMappingException(String message, SprocParameterMapping parameterMapping, Throwable cause) {
        super(message, cause);
        this.parameterMapping = parameterMapping;
    }

    public SprocParameterMappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public SprocParameterMappingException(String message, SprocParameterMapping parameterMapping) {
        this(message, parameterMapping, null);
    }
}

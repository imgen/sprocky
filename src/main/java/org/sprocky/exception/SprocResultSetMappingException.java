package org.sprocky.exception;

/**
 * Author: Imgen
 */
public class SprocResultSetMappingException extends RuntimeException {
    public SprocResultSetMappingException(String message, Throwable cause) {
        super(message, cause);
    }
}

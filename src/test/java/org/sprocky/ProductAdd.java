package org.sprocky;

/**
 * Author: Imgen
 *
 * This class is the Sproc Mapping Model Class for a hypothetical sproc ProductAdd.
 * Even better, you do not need to inherit from a stupid base class
 */
// Use LuxNameMappingConvention, class sprocName 'ProductAdd' will be mapped to sproc sprocName 'ProductAdd'
// Alternatively you could use @SprocModel(sprocName = "ProductAdd') which is redundant here
@SprocModel(name = "ProductAdd")
public class ProductAdd {
    // Will be mapped to sproc parameter "pId"
    // Alternatively, you could use @SprocParameter(sprocName = "pId") which is redundant here
    @SprocParameter(direction = SprocParameterDirection.Output)
    public Integer id;

    // Will be mapped to sproc parameter "pName"
    // Alternatively, you could use @SprocParameter(sprocName = "pName") which is redundant here
    public String name;

    // THE
    // REST
    // OF
    // THE
    // FIELDS
    // TO
    // MAP
    // TO
    // SPROC
    // PARAMETER
}

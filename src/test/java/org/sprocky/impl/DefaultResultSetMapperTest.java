package org.sprocky.impl;

import org.junit.Test;
import org.sprocky.ProductCategory;
import org.sprocky.util.ListBackedResultSet;

import java.sql.ResultSet;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.sprocky.util.MiscUtils.toList;
import static org.sprocky.util.TestUtils.assertSequenceEquals;

/** 
 * Author: Imgen
 */
public class DefaultResultSetMapperTest {

        /** 
    * 
    * Method: map(Class<T> type, ResultSet resultSet, LuxNameMappingConvention nameMappingConvention) 
    * 
    */ 
    @Test
    public void testMap() throws Throwable {
        List<Product> products = toList(new Product[]{
                new Product(3, "Razor", "ignored", "explicitly named", new ProductCategory(1, "mechnical")),
                new Product(4, "Kindle", "ignored", "explicitly named", new ProductCategory(2, "electrical device")),
                new Product(5, "Software", "ignored", "explicitly named", new ProductCategory(3, "software")),
                new Product(6, "SSD", "ignored", "explicitly named", new ProductCategory(4, "hard drive"))
        });
        ResultSet resultSet = new ListBackedResultSet<Product>(products, Product.class);

        List<Product> mappedProducts = new DefaultResultSetMapper().map(
                Product.class, resultSet, new LuxNameMappingConvention(), new DefaultTypeMapper());

        assertSequenceEquals(products, mappedProducts);

        // Test that the ignored field is actually ignored when doing mapping
        for (Product product:mappedProducts) {
            assertNull(product.ignored);
            assertNull(product.category);
        }
    } 
    
}

package org.sprocky.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Author: Imgen
 */
public class LuxNameMappingConventionTest {
    private LuxNameMappingConvention luxNameMappingConvention = new LuxNameMappingConvention();

    private class AnnouncementUpdate_01 {
        public String m_linkTitle;
    }

    @Test
    public void testFromClassToSproc() throws Exception {
        String className = "AnnouncementUpdate_01";
        String expected = "AnnouncementUpd#01";
        String actual = luxNameMappingConvention.fromClassToSproc(AnnouncementUpdate_01.class);
        assertEquals(expected, actual);
    }

    @Test
    public void testFromFieldToSprocParameter() throws Exception {
        String fieldName = "m_linkTitle";
        String expected = "pLinkTitle";
        String actual = luxNameMappingConvention.fromFieldToSprocParameter(
                AnnouncementUpdate_01.class.getField("m_linkTitle"));
        assertEquals(expected, actual);
    }

    @Test
    public void testFromFieldToResultSetColumn() throws Exception {
        String columnName = "m_linkTitle";
        String expected = "LinkTitle";
        String actual = luxNameMappingConvention.fromFieldToResultSetColumn(
                AnnouncementUpdate_01.class.getField("m_linkTitle"));
        assertEquals(expected, actual);
    }
}

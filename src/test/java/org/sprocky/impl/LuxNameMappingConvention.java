package org.sprocky.impl;

import org.apache.commons.lang3.StringUtils;
import org.sprocky.NameMappingConvention;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.sprocky.util.Constants.*;
import static org.sprocky.util.MiscUtils.*;

/**
 * Author: Imgen
 */
public class LuxNameMappingConvention implements NameMappingConvention {
    private static Map<String, String> classToSprocNameMap = new HashMap<String, String>();

    static {
        classToSprocNameMap.put("Update", "Upd");
        classToSprocNameMap.put("List", "Lst");
        classToSprocNameMap.put("Local", "Loc");
    }

    @Override
    public String fromClassToSproc(Class<?> type) {
        List<String> words = splitIdentifier(type.getSimpleName());
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i);
            if (classToSprocNameMap.containsKey(word)) {
                words.set(i, classToSprocNameMap.get(word));
            }
            else if (Character.isDigit(word.charAt(0))) {
                words.set(i, "#" + word);
            }
        }
        return StringUtils.join(words, EMPTY_STRING);
    }

    @Override
    public String fromFieldToSprocParameter(Field field) {
        String fieldName = field.getName();
        fieldName = removeIfStartsWith(fieldName, FIELD_NAME_PREFIX);
        String parameterName = SQL_PARAMETER_NAME_PREFIX + capitalizeFirstLetter(fieldName);
        return parameterName;
    }

    @Override
    public String fromFieldToResultSetColumn(Field field) {
        String fieldName = field.getName();
        fieldName = removeIfStartsWith(fieldName, FIELD_NAME_PREFIX);
        return capitalizeFirstLetter(fieldName);
    }
}

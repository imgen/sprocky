package org.sprocky.impl;

import org.junit.Test;
import org.sprocky.SQLDataType;
import org.sprocky.SprocParameterDirection;
import org.sprocky.SprocParameterMapping;
import org.sprocky.util.CallableStatementMock;

import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.sprocky.util.MiscUtils.now;

/**
 * Author: Imgen
 */
public class DefaultTypeMapperTest {
    private DefaultTypeMapper defaultTypeMapping = new DefaultTypeMapper();

    @Test
    public void testFromJavaToSqlType() throws Exception {
        assertEquals(SQLDataType.Int, defaultTypeMapping.fromJavaToSqlType(Integer.class));
        assertEquals(SQLDataType.Int, defaultTypeMapping.fromJavaToSqlType(int.class));

        assertEquals(SQLDataType.BigInt, defaultTypeMapping.fromJavaToSqlType(Long.class));
        assertEquals(SQLDataType.BigInt, defaultTypeMapping.fromJavaToSqlType(long.class));

        assertEquals(SQLDataType.SmallInt, defaultTypeMapping.fromJavaToSqlType(Short.class));
        assertEquals(SQLDataType.SmallInt, defaultTypeMapping.fromJavaToSqlType(short.class));

        assertEquals(SQLDataType.TinyInt, defaultTypeMapping.fromJavaToSqlType(Byte.class));
        assertEquals(SQLDataType.TinyInt, defaultTypeMapping.fromJavaToSqlType(byte.class));

        assertEquals(SQLDataType.Bit, defaultTypeMapping.fromJavaToSqlType(Boolean.class));
        assertEquals(SQLDataType.Bit, defaultTypeMapping.fromJavaToSqlType(boolean.class));

        assertEquals(SQLDataType.Double, defaultTypeMapping.fromJavaToSqlType(Double.class));
        assertEquals(SQLDataType.Double, defaultTypeMapping.fromJavaToSqlType(double.class));

        assertEquals(SQLDataType.Float, defaultTypeMapping.fromJavaToSqlType(Float.class));
        assertEquals(SQLDataType.Float, defaultTypeMapping.fromJavaToSqlType(float.class));

        assertEquals(SQLDataType.Varbinary, defaultTypeMapping.fromJavaToSqlType(Byte[].class));
        assertEquals(SQLDataType.Varbinary, defaultTypeMapping.fromJavaToSqlType(byte[].class));

        assertEquals(SQLDataType.Date, defaultTypeMapping.fromJavaToSqlType(Date.class));
        assertEquals(SQLDataType.Date, defaultTypeMapping.fromJavaToSqlType(java.util.Date.class));

        assertEquals(SQLDataType.Timestamp, defaultTypeMapping.fromJavaToSqlType(Timestamp.class));
        assertEquals(SQLDataType.Time, defaultTypeMapping.fromJavaToSqlType(Time.class));
        assertEquals(SQLDataType.Varchar, defaultTypeMapping.fromJavaToSqlType(String.class));
    }

    @Test
    public void testSetSprocParameter() throws Exception {
        CallableStatement callableStatement = new CallableStatementMock();
        Field dummyField = Product.class.getField("id");
        // Int parameter
        SprocParameterMapping mapping = new SprocParameterMapping("pId", dummyField, Integer.class,
                3, SQLDataType.Int);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // NVarchar parameter
        mapping = new SprocParameterMapping("pName", dummyField, String.class,
                "sprocName", SQLDataType.NVarchar);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // java.util.Date parameter
        mapping = new SprocParameterMapping("pStartDate", dummyField, java.util.Date.class,
                new java.util.Date(), SQLDataType.Date);

        // java.sql.Date parameter
        mapping = new SprocParameterMapping("pStartDate", dummyField, Date.class,
                new Date(now()), SQLDataType.Date);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // Timestamp parameter
        mapping = new SprocParameterMapping("pStartTime", dummyField, Timestamp.class,
                new Timestamp(now()), SQLDataType.Timestamp);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // Time parameter
        mapping = new SprocParameterMapping("pStartTime", dummyField, Time.class,
                new Time(now()), SQLDataType.Time);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // Byte array parameter
        mapping = new SprocParameterMapping("pBinaryData", dummyField, byte[].class,
                new byte[] {13, 35, 28}, SQLDataType.Varbinary);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);
        mapping = new SprocParameterMapping("pBinaryData", dummyField, Byte[].class,
                new Byte[] {13, 35, 28}, SQLDataType.Varbinary);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // Null parameter
        mapping = new SprocParameterMapping("pId", dummyField,
                Integer.class, null, SQLDataType.Int);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);

        // Out parameter
        mapping = new SprocParameterMapping("pId", null, Integer.class,
                3, SQLDataType.Int, SprocParameterDirection.Output);
        defaultTypeMapping.setSprocParameter(callableStatement, mapping);
    }
}

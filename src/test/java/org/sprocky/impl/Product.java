package org.sprocky.impl;

import org.sprocky.ProductCategory;
import org.sprocky.ResultSetColumn;
import org.sprocky.ResultSetColumnIgnore;

/**
 * Author: Imgen
 */
public class Product {
    // Will be mapped to result set column "Id"
    // Alternatively, you could use @ResultSetColumn(sprocName = "Id") which is redundant here
    public Integer id;

    // Will be mapped to result set column "pName"
    // Alternatively, you could use @ResultSetColumn(sprocName = "Name") which is redundant here
    public String name;

    @ResultSetColumnIgnore
    public Object ignored;

    @ResultSetColumn(name = "ExplicitlyNamed")
    public String explicitlyNamingNeeded;

    /**
     * This is a complex type that cannot be mapped by the default TypeMapper implementation, so it will
     * be ignored by both the DefaultResultSetMapper and SprocModelMapper
     */
    public ProductCategory category;

    // THE
    // REST
    // OF
    // THE
    // FIELDS
    // TO
    // MAP
    // TO
    // RESULT
    // SET
    // COLUMN

    public Product() {}

    public Product(int id, String name) {
        this(id, name, null, null, null);
    }

    public Product(int id, String name, Object ignored, String explicitlyNamingNeeded) {
        this(id, name, ignored, explicitlyNamingNeeded, null);
    }

    public Product(int id, String name, Object ignored, String explicitlyNamingNeeded, ProductCategory category) {
        this.id = id;
        this.name = name;
        this.ignored = ignored;
        this.explicitlyNamingNeeded = explicitlyNamingNeeded;
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (explicitlyNamingNeeded != null ? !explicitlyNamingNeeded.equals(product.explicitlyNamingNeeded) :
                product.explicitlyNamingNeeded != null)
            return false;
        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (explicitlyNamingNeeded != null ? explicitlyNamingNeeded.hashCode() : 0);
        return result;
    }
}
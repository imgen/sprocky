package org.sprocky.impl;

import org.junit.Test;
import org.sprocky.*;
import org.sprocky.exception.SprocParameterMappingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.sprocky.util.MiscUtils.toList;
import static org.sprocky.util.TestUtils.assertSequenceEquals;

/**
 * Author: Imgen
 */
public class SprocModelMapperTest {
    @Test
    public void testMapWithNoAnnotation() throws Exception {
        ProductUpdate01 productUpdate01 = new ProductUpdate01();
        productUpdate01.id = 3;
        productUpdate01.name = "named";
        SprocModelMapping mapping = SprocModelMapper.map(
                productUpdate01, new LuxNameMappingConvention());

        assertEquals("ProductUpd#01", mapping.sprocName);

        Class<?> type = ProductUpdate01.class;
        assertSequenceEquals(toList(new SprocParameterMapping[]{
                new SprocParameterMapping("pId", type.getField("id"), Integer.class,
                        productUpdate01.id, SQLDataType.Int),
                new SprocParameterMapping("pName", type.getField("name"), String.class,
                        productUpdate01.name, SQLDataType.Varchar)
            }),
            mapping.parameters);
    }

    @Test
    public void testMapWithAnnotation() throws Exception {
        ProductAdd productAdd = new ProductAdd();
        productAdd.name = "named";
        SprocModelMapping mapping = SprocModelMapper.map(
                productAdd, new LuxNameMappingConvention());

        assertEquals("ProductAdd", mapping.sprocName);

        Class<?> type = ProductAdd.class;
        assertSequenceEquals(toList(new SprocParameterMapping[]{
                new SprocParameterMapping("pId", type.getField("id"), Integer.class,
                        null, SQLDataType.Int, SprocParameterDirection.Output),
                new SprocParameterMapping("pName", type.getField("name"), String.class,
                        productAdd.name, SQLDataType.Varchar)
            }),
            mapping.parameters);
    }

    private class IgnorantModel {
        public int id;

        @SprocParameterIgnore
        public double computedTotalPrice;

        public Product product;
    }

    @Test
    public void testMapWithSprocParameterIgnoreAnnotation() throws NoSuchFieldException {
        IgnorantModel model = new IgnorantModel();
        SprocModelMapping mapping = SprocModelMapper.map(
                model, new LuxNameMappingConvention());
        Class<?> type = IgnorantModel.class;
        assertSequenceEquals(toList(new SprocParameterMapping[]{
                new SprocParameterMapping("pId",
                        type.getField("id"),
                        int.class,
                        0,
                        SQLDataType.Int),
            }),
            mapping.parameters);
    }

    private class DuplicateMappedModel {
        @SprocParameter(name = "pSomeParameter")
        public int field;

        @SprocParameter(name = "pSomeParameter")
        public String anotherField;
    }
    @Test
    public void testDuplicationHandling() throws Exception {
        DuplicateMappedModel model = new DuplicateMappedModel();
        try {
            SprocModelMapping mapping = SprocModelMapper.map(
                    model, new LuxNameMappingConvention());
            fail("An exception should be thrown when there is duplication in mapping");
        }
        catch (SprocParameterMappingException e) {
        }

    }
}

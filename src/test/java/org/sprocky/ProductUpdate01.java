package org.sprocky;

/**
 * Author: Imgen
 *
 * This class is the Sproc Mapping Model Class for a hypothetical sproc ProductUpd#01.
 * Even better, you do not need to inherit from a stupid base class
 */
// Use LuxNameMappingConvention, class sprocName 'ProductUpdate01' will be mapped to sproc sprocName 'ProductUpd#01'
// Alternatively you could use @SprocModel(sprocName = "ProductUpd#01') which is redundant here
public class ProductUpdate01 {
    // Will be mapped to sproc parameter "pId"
    // Alternatively, you could use @SprocParameter(sprocName = "pId") which is redundant here
    public Integer id;

    // Will be mapped to sproc parameter "pName"
    // Alternatively, you could use @SprocParameter(sprocName = "pName") which is redundant here
    public String name;

    // THE
    // REST
    // OF
    // THE
    // FIELDS
    // TO
    // MAP
    // TO
    // SPROC
    // PARAMETER
}
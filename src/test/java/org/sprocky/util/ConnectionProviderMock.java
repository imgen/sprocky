package org.sprocky.util;

import org.sprocky.ConnectionProvider;
import org.sprocky.exception.DataAccessException;

import java.sql.Connection;

/**
 * Author: Imgen
 */
public class ConnectionProviderMock implements ConnectionProvider {
    public ConnectionMock connection = new ConnectionMock();

    @Override
    public Connection acquire() throws DataAccessException {
        return connection;
    }

    @Override
    public void release(Connection connection) throws DataAccessException {
    }
}

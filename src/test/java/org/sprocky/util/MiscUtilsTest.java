package org.sprocky.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.sprocky.util.MiscUtils.*;
import static org.sprocky.util.TestUtils.assertSequenceEquals;

/**
 * Author: Imgen
 */
public class MiscUtilsTest {
    @Test
    public void testSplitIdentifier()  {
        String identifier = "AddAnnouncement01";
        Collection<String> expected = toList(new String[]{"Add", "Announcement", "01"});
        Collection<String> actual = splitIdentifier(identifier);
        Assert.assertNotNull(actual);
        assertSequenceEquals(expected, actual);

        identifier = "AddAnnouncement_01";
        expected = toList(new String[]{"Add", "Announcement", "01"});
        actual = splitIdentifier(identifier);
        Assert.assertNotNull(actual);
        assertSequenceEquals(expected, actual);

        identifier = "add_Announcement_01";
        expected = toList(new String[]{"add", "Announcement", "01"});
        actual = splitIdentifier(identifier);
        Assert.assertNotNull(actual);
        assertSequenceEquals(expected, actual);
    }

    @Test
    public void testGetHexString() {
        byte[] bytes = new byte[] {0x13, 0x1A, (byte) 0xA3};
        String expected = "131AA3";
        String actual = getHexString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void testBuildSprocCallSql() {
        // Test no parameter, no return value
        String sql = buildSprocCallSql("VoidSproc", 0, false);
        assertEquals("{ call VoidSproc () }", sql);

        // Test no parameter, has return value
        sql = buildSprocCallSql("SomeSproc", 0, true);
        assertEquals("{ ? = call SomeSproc () }", sql);

        // Test 3 parameters and a return value
        sql = buildSprocCallSql("SomeSproc", 3, true);
        assertEquals("{ ? = call SomeSproc (?,?,?) }", sql);
    }

    @Test
    public void testGetCorrespondingPrimitiveType() {
        assertEquals(int.class, getCorrespondingPrimitiveType(Integer.class));
        assertEquals(int[].class, getCorrespondingPrimitiveType(Integer[].class));
    }

}

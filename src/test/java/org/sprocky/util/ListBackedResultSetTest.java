package org.sprocky.util;

import org.junit.Test;
import org.sprocky.impl.LuxNameMappingConvention;
import org.sprocky.impl.Product;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.sprocky.util.MiscUtils.*;

/**
 * Author: Imgen
 */
public class ListBackedResultSetTest { 

    @Test
    public void testEmptyList() throws SQLException {
        ListBackedResultSet<Product> productListBackedResultSet =
                new ListBackedResultSet<Product>(new ArrayList<Product>(), Product.class);

        assertFalse(productListBackedResultSet.next());
    }

    @Test
    public void testWithNonEmptyList() throws SQLException {
        List<Product> products = toList(new Product[]{
                new Product(1, "Razor"),
                new Product(2, "Sizzle"),
                new Product(3, "Dizzle")
        });

        ListBackedResultSet<Product> productListBackedResultSet =
                new ListBackedResultSet<Product>(products, Product.class);

        assertTrue(productListBackedResultSet.next());
        assertEquals(1, productListBackedResultSet.getObject("Id"));
        assertEquals("Razor", productListBackedResultSet.getObject("Name"));

        assertTrue(productListBackedResultSet.next());
        assertEquals(2, productListBackedResultSet.getObject("Id"));
        assertEquals("Sizzle", productListBackedResultSet.getObject("Name"));

        assertTrue(productListBackedResultSet.next());
        assertEquals(3, productListBackedResultSet.getObject("Id"));
        assertEquals("Dizzle", productListBackedResultSet.getObject("Name"));

        assertFalse(productListBackedResultSet.next());
    }

    @Test
    public void testGetMetaData() throws SQLException {
        List<Product> products = toList(new Product[]{
                new Product(1, "Razor"),
                new Product(2, "Sizzle"),
                new Product(3, "Dizzle")
        });

        ListBackedResultSet<Product> productListBackedResultSet =
                new ListBackedResultSet<Product>(products, Product.class);

        ResultSetMetaDataMock resultSetMetaData = (ResultSetMetaDataMock)productListBackedResultSet.getMetaData();
        assertNotNull(resultSetMetaData);
        assertEquals(getApplicableFields(Product.class).length - 1, resultSetMetaData.getColumnCount());
        List<String> fieldNames = getFieldNames(Product.class);
        LuxNameMappingConvention luxNameMappingConvention = new LuxNameMappingConvention();

        assertTrue(resultSetMetaData.columnNames.contains("Id"));
        assertTrue(resultSetMetaData.columnNames.contains("Name"));
        assertTrue(resultSetMetaData.columnNames.contains("ExplicitlyNamed"));
        assertFalse(resultSetMetaData.columnNames.contains("Ignored"));
    }
}

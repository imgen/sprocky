package org.sprocky;

/**
 * Author: Imgen
 */
public class ProductCategory {
    public static final ProductCategory Cheap = new ProductCategory(3, "Cheap products");
    public static final ProductCategory Expensive = new ProductCategory(4, "Expensive products");

    public ProductCategory() {}
    public ProductCategory(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    // Will be mapped to result set column "Id"
    // Alternatively, you could use @ResultSetColumn(sprocName = "Id") which is redundant here
    public Integer id;

    // Will be mapped to result set column "pName"
    // Alternatively, you could use @ResultSetColumn(sprocName = "Name") which is redundant here
    public String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCategory that = (ProductCategory) o;

        if (!id.equals(that.id)) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    // THE
    // REST
    // OF
    // THE
    // FIELDS
    // TO
    // MAP
    // TO
    // RESULT
    // SET
    // COLUMN
}

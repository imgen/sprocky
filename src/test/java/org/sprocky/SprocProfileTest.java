package org.sprocky;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.junit.Before;
import org.junit.Test;
import org.sprocky.impl.DefaultResultSetMapper;
import org.sprocky.impl.DefaultTypeMapper;
import org.sprocky.impl.LuxNameMappingConvention;
import org.sprocky.impl.Product;
import org.sprocky.util.CallableStatementMock;
import org.sprocky.util.ConnectionMock;
import org.sprocky.util.ConnectionProviderMock;
import org.sprocky.util.ListBackedResultSet;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.sprocky.util.MiscUtils.toList;
import static org.sprocky.util.TestUtils.assertSequenceEquals;


class SprocReturnValue {
    public static final int Succeeded = 100;
    public static final int Failed = -100;
}

class SprocScalarValue {
    public Object value;

    public SprocScalarValue(Object value) {
        this.value = value;
    }

    public static <T> ListBackedResultSet<SprocScalarValue> getScalarResultSet(T value, Class<T> type) {
        List<SprocScalarValue> data = new ArrayList<SprocScalarValue>();
        data.add(new SprocScalarValue(value));
        return  new ListBackedResultSet<SprocScalarValue>(data, SprocScalarValue.class);
    }
}

/**
 * SprocProfile Tester.
 *
 */
public class SprocProfileTest {

    private SprocProfile sprocProfile = new SprocProfile("Lux");
    private CallableStatementMock callableStatementMock = new CallableStatementMock();

    public SprocProfileTest() {
        // Configure sproc profile
        sprocProfile.setDialect(SQLDialect.SQLServer);
        ConnectionProviderMock connectionProvider = new ConnectionProviderMock();
        connectionProvider.connection = new ConnectionMock();
        this.callableStatementMock.returnValue = SprocReturnValue.Succeeded;
        connectionProvider.connection.callableStatement = this.callableStatementMock;
        sprocProfile.setConnectionProvider(connectionProvider);
        sprocProfile.setNameMappingConvention(new LuxNameMappingConvention());
        sprocProfile.setResultSetMapper(new DefaultResultSetMapper());
        sprocProfile.setErrorHandler(new SprocErrorHandler() {
            @Override
            public void handle(Throwable exception) throws Throwable {
                throw exception;    // simply rethrow it
            }
        });
    }

    @Before
    public void resetCallableStatementMock() {
        this.callableStatementMock.resultSets = new ArrayList<ResultSet>();
        this.callableStatementMock.outputParameterValueMap = new HashMap<String, Object>();
    }

    /**
     *
     * Method: executeNonQuery(String sprocName)
     *
     */
    @Test
    public void testExecuteNonQuerySprocName() throws Throwable {
        // executeNonQuery(String sprocName) is for executing sproc
        // that accepts no parameters and returns no result set
        // This is a one-liner
        int ret = sprocProfile.executeNonQuery("DoNothing");
        assertEquals(ret, SprocReturnValue.Succeeded);
    }

    /**
     *
     * Method: executeNonQuery(String sprocName, Object singleParameter)
     *
     */
    @Test
    public void testExecuteNonQueryForSprocNameSingleParameter() throws Throwable {
        // executeNonQuery(String sprocName, Object singleParameter) is for executing
        // sproc that only accepts only one parameter and returns no result set
        // This is a one-liner
        int ret = sprocProfile.executeNonQuery("ProductExists", 3);      // pProductId = 3
        boolean productExists = ret == SprocReturnValue.Succeeded;
        assertTrue(productExists);
    }

    /**
     *
     * Method: executeNonQuery(TSprocModel sprocModel)
     *
     */
    @Test
    public void testExecuteNonQuerySprocModel() throws Throwable {
        // executeNonQuery(TSprocModel sprocModel) is for executing
        // sproc that has multiple parameters (hence a Sproc Model Class)
        // and returns no result set
        ProductUpdate01 productUpdateModel = new ProductUpdate01();
        productUpdateModel.id = 3;
        productUpdateModel.name = "2X3";
        int ret = sprocProfile.executeNonQuery(productUpdateModel);
        assertEquals(ret, SprocReturnValue.Succeeded);

        ProductAdd productAddModel = new ProductAdd();
        productAddModel.name = "2X3";
        Integer productId = 4;

        this.callableStatementMock.outputParameterValueMap.put("pId", productId);
        ret = sprocProfile.executeNonQuery(productAddModel);
        // The output parameter pId will be mapped to the id field as it's marked as Output parameter
        assertEquals(ret, SprocReturnValue.Succeeded);
        assertEquals(productId, productAddModel.id);
    }

    /**
     *
     * Method: executeScalar(String sprocName)
     *
     */
    @Test
    public void testExecuteScalarSprocName() throws Throwable {
        // executeScalar(String sprocName) is for executing
        // sproc accepts no parameters and returns scalar value
        Integer productCount = 105;
        this.callableStatementMock.resultSets.add(SprocScalarValue.getScalarResultSet(productCount, Integer.class));
        ScalarSprocResult result = sprocProfile.executeScalar("ProductCount");
        assertEquals(SprocReturnValue.Succeeded, result.returnValue);
        assertEquals(productCount, (Integer)result.scalarValue);
    }

    /**
     *
     * Method: executeScalar(String sprocName, Object singleParameter)
     *
     */
    @Test
    public void testExecuteScalarForSprocNameSingleParameter() throws Throwable {
        // executeScalar(String sprocName, Object singleParameter) is for executing
        // sproc accepts one parameters and returns scalar value
        Integer productCount = 105;
        this.callableStatementMock.resultSets.add(SprocScalarValue.getScalarResultSet(productCount, Integer.class));
        ScalarSprocResult result = sprocProfile.executeScalar("ProductCountByCategory",
                ProductCategory.Cheap.id);
        assertEquals(SprocReturnValue.Succeeded, result.returnValue);
        assertEquals(productCount, (Integer)result.scalarValue);
    }

    /**
     *
     * Method: executeScalar(TSprocModel sprocModel)
     *
     */
    @Test
    public void testExecuteScalarSprocModel() throws Throwable {
        // executeScalar(TSprocModel sprocModel) is for executing
        // sproc accepts multiple parameters and returns scalar value
        ProductAdd productAddModel = new ProductAdd();
        productAddModel.name = "2X3";
        Integer productId = 105;
        this.callableStatementMock.resultSets.add(SprocScalarValue.getScalarResultSet(productId, Integer.class));
        this.callableStatementMock.outputParameterValueMap.put("pId", productId);
        ScalarSprocResult result = sprocProfile.executeScalar(productAddModel);
        // Here we assume the sproc use both Output parameter and scalar result set to
        // return the added product's id
        assertEquals(productAddModel.id, result.scalarValue);
    }

    /**
     *
     * Method: executeQuery(String sprocName, Class<TResult> sqlType)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameType() throws Throwable {
        // executeQuery(String sprocName, Class<TResult> sqlType) is for executing
        // sproc accepts no parameters and returns single result set
        List<Product> products = toList(new Product[]{
                new Product(3, "Fan"),
                new Product(4, "Word")
        });
        ListBackedResultSet<Product> resultSet = new ListBackedResultSet<Product>(products, Product.class);
        this.callableStatementMock.resultSets.add(resultSet);

        QuerySprocResult<Product> querySprocResult = sprocProfile.executeQuery("ProductLst", Product.class);
        assertEquals(SprocReturnValue.Succeeded, querySprocResult.returnValue);
        assertSequenceEquals(products, querySprocResult.queryResult);
    }

    /**
     *
     * Method: executeQuery(String sprocName, Class<?>... resultSetTypes)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameResultSetTypes() throws Throwable {
        // executeQuery(String sprocName, Class<?>... resultSetTypes) is for executing
        // sproc accepts no parameters and returns multiple result set
        // resultSetTypes is a list of the expected types to be mapped from the result set
        List<Product> products = toList(new Product[]{
                new Product(3, "Fan"),
                new Product(4, "Word")
        });
        ListBackedResultSet<Product> productResultSet =
                new ListBackedResultSet<Product>(products, Product.class);
        this.callableStatementMock.resultSets.add(productResultSet);

        List<ProductCategory> productCategories = toList(new ProductCategory[]{
                new ProductCategory(1, "Hardware"),
                new ProductCategory(2, "Books")
        });
        ListBackedResultSet<ProductCategory> productCategoryResultSet =
                new ListBackedResultSet<ProductCategory>(productCategories, ProductCategory.class);
        this.callableStatementMock.resultSets.add(productCategoryResultSet);

        QuerySprocResult querySprocResult = sprocProfile.executeQuery(
                        "ProductAndProductCategoryLst",
                        new Class<?>[]{
                            Product.class,
                            ProductCategory.class
                        });

        assertEquals(SprocReturnValue.Succeeded, querySprocResult.returnValue);
        List result = querySprocResult.queryResults;
        List<?> productObjects = (List<?>)result.get(0);
        Collection<Product> returnedProducts = CollectionUtils.collect(productObjects,
                new Transformer<Object, Product>() {
                    @Override
                    public Product transform(Object o) {
                        return (Product)o;
                    }
                });
        assertSequenceEquals(products, returnedProducts);

        List<?> productCategoryObjects = (List<?>)result.get(1);
        Collection<ProductCategory> returnedProductCategories = CollectionUtils.collect(productCategoryObjects,
                new Transformer<Object, ProductCategory>() {
                    @Override
                    public ProductCategory transform(Object o) {
                        return (ProductCategory)o;
                    }
                });
        assertSequenceEquals(productCategories, returnedProductCategories);
    }

    /**
     *
     * Method: executeQuery(String sprocName, ResultSetProcessor resultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameResultSetProcessor() throws Throwable {
        // executeQuery(String sprocName, ResultSetProcessor resultSetProcessor) is for executing
        // sproc accepts no parameters and returns single result set
        // using ResultSetProcessor we can avoid creating a class for mapping result set if using anonymous class
        final List<Product> products = toList(new Product[]{
                new Product(3, "Fan"),
                new Product(4, "Word")
        });
        ListBackedResultSet<Product> productResultSet =
                new ListBackedResultSet<Product>(products, Product.class);
        this.callableStatementMock.resultSets.add(productResultSet);
        int returnValue = sprocProfile.executeQuery("ProductLst", new ResultSetProcessor() {
            @Override
            public void process(ResultSet resultSet) throws Throwable {
                List<Product> returnedProducts = sprocProfile.getResultSetMapper().map(Product.class,
                        resultSet,
                        new LuxNameMappingConvention(),
                        new DefaultTypeMapper());
                assertSequenceEquals(products, returnedProducts);
            }
        });
        assertEquals(SprocReturnValue.Succeeded, returnValue);
    }

    /**
     *
     * Method: executeQuery(String sprocName, MultiResultSetProcessor multiResultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameMultiResultSetProcessor() throws Throwable {
        // executeQuery(String sprocName, MultiResultSetProcessor multiResultSetProcessor) is for executing
        // sproc accepts no parameters and returns multiple result set
        // using MultiResultSetProcessor we can avoid creating a class for mapping result set if using anonymous class
        final List<Product> products = toList(new Product[]{
                new Product(3, "Fan"),
                new Product(4, "Word")
        });
        ListBackedResultSet<Product> productResultSet =
                new ListBackedResultSet<Product>(products, Product.class);
        this.callableStatementMock.resultSets.add(productResultSet);

        final List<ProductCategory> productCategories = toList(new ProductCategory[]{
                new ProductCategory(1, "Hardware"),
                new ProductCategory(2, "Books")
        });
        ListBackedResultSet<ProductCategory> productCategoryResultSet =
                new ListBackedResultSet<ProductCategory>(productCategories, ProductCategory.class);
        this.callableStatementMock.resultSets.add(productCategoryResultSet);
        int ret = sprocProfile.executeQuery("ProductAndProductCategoryLst", new MultiResultSetProcessor() {
            @Override
            public void process(ResultSet resultSet, int resultSetIndex) throws Throwable {
                if (resultSetIndex == 0) { // Product result set
                    List<Product> returnedProducts = sprocProfile.getResultSetMapper().map(Product.class,
                            resultSet,
                            new LuxNameMappingConvention(),
                            new DefaultTypeMapper());
                    assertSequenceEquals(products, returnedProducts);
                }
                else if (resultSetIndex == 1) { // Product category result set
                    List<ProductCategory> returnedProductCategories = sprocProfile.getResultSetMapper().
                            map(ProductCategory.class,
                            resultSet,
                            new LuxNameMappingConvention(),
                            new DefaultTypeMapper());
                    assertSequenceEquals(productCategories, returnedProductCategories);
                }
            }
        });
        assertEquals(SprocReturnValue.Succeeded, ret);
    }

    /**
     *
     * Method: executeQuery(String sprocName, Object singleParameter, Class<TResult> sqlType)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameSingleParameterType() throws Exception {

    }

    /**
     *
     * Method: executeQuery(String sprocName, Object singleParameter, Class<?>... resultSetTypes)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameSingleParameterResultSetTypes() throws Exception {

    }

    /**
     *
     * Method: executeQuery(String sprocName, Object singleParameter, ResultSetProcessor resultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameSingleParameterResultSetProcessor() throws Exception {

    }

    /**
     *
     * Method: executeQuery(String sprocName, Object singleParameter, MultiResultSetProcessor multiResultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocNameSingleParameterMultiResultSetProcessor() throws Exception {

    }

    /**
     *
     * Method: executeQuery(TSprocModel sprocModel, Class<TResult> sqlType)
     *
     */
    @Test
    public void testExecuteQueryForSprocModelType() throws Exception {

    }

    /**
     *
     * Method: executeQuery(TSprocModel sprocModel, Class<?>... resultSetTypes)
     *
     */
    @Test
    public void testExecuteQueryForSprocModelResultSetTypes() throws Exception {

    }

    /**
     *
     * Method: executeQuery(TSprocModel sprocModel, ResultSetProcessor resultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocModelResultSetProcessor() throws Exception {

    }

    /**
     *
     * Method: executeQuery(TSprocModel sprocModel, MultiResultSetProcessor multiResultSetProcessor)
     *
     */
    @Test
    public void testExecuteQueryForSprocModelMultiResultSetProcessor() throws Exception {

    }

}
